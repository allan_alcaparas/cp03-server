const passport = require('passport');
const JWStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('./models/User');

const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = 'secret';

passport.use(new JWStrategy(opts,function(jwt_payLoad,done){
	User.findOne({_id:jwt_payLoad.id},function(err,user){
		if (err) {
			return done(err,false)
		}
		if (user) {
			return done(null,user)
		} else {
			return done(null,false)
		}
	})
}))

module.exports = passport