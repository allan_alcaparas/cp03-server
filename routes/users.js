const router = require('express').Router();
const User = require('./../models/User');
const bcrypt = require('bcrypt');
const passport = require('passport');
require('./../passport-setup');
const jwt = require('jsonwebtoken');

router.post('/register',(req,res,next)=>{

	let firstname = req.body.firstname
	let lastname = req.body.lastname
	let email = req.body.email
	let password = req.body.password
	let confirmPassword = req.body.confirmPassword

	if (!firstname || !lastname || !email || !password || !confirmPassword) {
		return res.status(400).send({
			message: "Complete the required fields."
		})
	}

	if (password.length<6) {
		return res.status(400).send({
			message: "Password must be at least 6 characters."
		})
	} else {
		if (password !== confirmPassword) {
			return res.status(400).send({
				message: "Password do not matched."
			})
		}
	}

	User.findOne({email:email})
	.then(user=>{
		if (user) {
			return res.status(400).send({
				message: "Use another email. Email is already in use."
			})
		} else {
			const saltRounds = 5;
			bcrypt.genSalt(saltRounds, function(err,salt){
				bcrypt.hash(password, salt, function(err,hash){
					User.create({
						firstname,
						lastname,
						email,
						password:hash
					})
					.then(user=>{
					return res.send(user)
					})	
				})
			})
		}
	})
})

router.post('/profile',passport.authenticate('jwt',{session:false}),function(req,res){
	res.send(req.user)
})

router.post('/login',(req,res,next)=>{
	
})

module.exports = router;