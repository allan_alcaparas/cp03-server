const router = require('express').Router();
const Conductor = require('./../models/Conductor');

router.post('/',(req,res,next)=>{
	Conductor.create(req.body)
	.then(conductor=>{
		return res.send(conductor)
	})
})


module.exports = router;