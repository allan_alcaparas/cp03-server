const router = require('express').Router();
const Destination = require('./../models/Destination');

router.post('/',(req,res,next)=>{
	Destination.create(req.body)
	.then(destination=>{
		return res.send(destination)
	})
})


module.exports = router;