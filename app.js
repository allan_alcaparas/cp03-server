const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

// initialize server

const app = express();
const port = process.env.PORT || 8000

// middlewares
app.use(bodyParser.json())
app.use(cors());
app.use(passport.initialize());

mongoose.connect('mongodb+srv://admin:admin@cluster0-r6hgf.mongodb.net/test?retryWrites=true&w=majority',()=>{
	console.log("Connected to database")
});

/*mongoose.connect('mongodb://localhost/bookingsystem',()=>{
	console.log("Connected to database")
});*/

app.use('/users',require('./routes/users'));
app.use('/destinations',require('./routes/destinations'));
app.use('/buses',require('./routes/buses'));
app.use('/drivers',require('./routes/drivers'));
app.use('/conductors',require('./routes/conductors'));
app.use('/origins',require('./routes/origins'));

app.listen(port,()=>{
	console.log(`Listening in port ${port}`)
})