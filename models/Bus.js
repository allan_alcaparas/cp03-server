const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const BusSchema =  new Schema({
	model: {
		type: String,
		required: true
	},
	capacity: {
		type: Number,
		required: true
	},
	plateNo: {
		type: String,
		required: true
	}

})

const Bus = mongoose.model('Bus', BusSchema);
module.exports = Bus;