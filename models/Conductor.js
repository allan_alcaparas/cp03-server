const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ConductorSchema =  new Schema({
	firstname: {
		type: String,
		required: true
	},
	lastname: {
		type: String,
		required: true
	},
	nbi: {
		type: String,
		required: true
	}

})

const Conductor = mongoose.model('Conductor', ConductorSchema);
module.exports = Conductor;