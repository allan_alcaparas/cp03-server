const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const DriverSchema =  new Schema({
	firstname: {
		type: String,
		required: true
	},
	lastname: {
		type: String,
		required: true
	},
	licenseNo: {
		type: String,
		required: true
	}

})

const Driver = mongoose.model('Driver', DriverSchema);
module.exports = Driver;