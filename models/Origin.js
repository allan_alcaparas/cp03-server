const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const OriginSchema =  new Schema({
	location: {
		type: String,
		required: true
	}

})

const Origin = mongoose.model('Origin', OriginSchema);
module.exports = Origin;