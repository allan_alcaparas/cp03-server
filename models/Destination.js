const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const DestinationSchema =  new Schema({
	location: {
		type: String,
		required: true
	}

})

const Destination = mongoose.model('Destination', DestinationSchema);
module.exports = Destination;